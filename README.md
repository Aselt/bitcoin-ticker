# Bitcoin Ticker

In this project in order to grab Bitcoin and other cryptocurrency data (Ethereum, Litecoins), and allows our users to check the latest prices(USD, GB, EU) of their cryptocurrency. I have got the [Web site](https://bitcoinaverage.com/) which is one of the largest or as they say one of the oldest sources of Bitcoin data, and I have used their API in my Bitcoin Ticker Application.

This Application built on **Node.js**, **Express framework** and **Bootstrap4**.

How Install and Run this project in your browser:
```
git clone https://gitlab.com/Aselt/bitcoin-ticker.git
cd project_folder
npm install **install all node_module packages**
node index.js **to run project in localhost** OR
nodemon index.js
```
## Check Bitcoin prices
In **_localhost:3001/_** you can check bitcoin price.

## Convert Bitcoin quantity
In **_localhost:3001/convert_** it takes a quantity of bitcoins and it converted into US dollars or whatever it is that you choose.
