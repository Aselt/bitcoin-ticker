//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));


app.get("/", function(req, res){
  res.sendFile(__dirname + "/index.html");
});

app.post("/", function(req, res){
  //console.log(req.body.crypto);
  var crypto = req.body.crypto;
  var fiat = req.body.fiat;
  var baseURL = "https://apiv2.bitcoinaverage.com/indices/global/ticker/";
  var finalURL = baseURL + crypto + fiat;  //this url will dynamic

    request(finalURL, function(error, response, body){
      //console.log(body);
      var data = JSON.parse(body);
      var price = data.last; //last price bitcoin
      //console.log(price);
      var currentDate = data.display_timestamp;
      res.write("<p>Today date is " + currentDate + "</p>");
      res.write("<h1>The last price of "+ crypto + " is "  + price + fiat + "</h1>");
      res.send();
    });

});

// Price Conversion Section

app.get("/convert", function(req, res){
  res.sendFile(__dirname + "/convert.html");
});

app.post("/convert", function(req, res){
  var crypto = req.body.crypto;
  var fiat = req.body.fiat;
  var amount = req.body.amount;

  var options = {
    url: "https://apiv2.bitcoinaverage.com/convert/global",
    method: "GET",
    qs: {
      from: crypto,
      to: fiat,
      amount: amount
    }
  };

  request(options, function(error, response, body){
    var data = JSON.parse(body);
    var price = data.price;
    //console.log(price);
    var todayDate = data.time;

    res.write("<p>Today date is " + todayDate + "</p>");
    res.write("<h1>The  "+ amount + crypto + " is currently worth "  + price + fiat + "</h1>");
    res.send();
  });
});


//Port Section
app.listen(3001, function(){
  console.log("Server is running 3001 port");
});
